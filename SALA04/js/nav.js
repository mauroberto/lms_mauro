function close_nav(nav){
  var elem = document.getElementById(nav);
  if(!elem) return;

  elem.style.display = "none";
  var caret = document.getElementById(nav+"-caret");
  caret.className = "fa fa-caret-down";
}

function open_nav(nav){
  var elem = document.getElementById(nav);
  if(!elem) return;

  if(elem.style.display == "block") {
    close_nav(nav);
    return;
  }

  var navs = document.getElementsByClassName('dropnav');

  for(var i = 0; i<navs.length; i++){
    close_nav(navs[i].id);
  }

  var caret = document.getElementById(nav+"-caret");
  caret.className = "fa fa-caret-up";

  elem.style.display = "block";
}

function toggle_xs_menu(menu){
  var elem = document.getElementById("sectionxs_"+menu);
  console.log(elem);
  if(!elem) return;
  if(elem.style.display == "none"){
    elem.style.display = "block";
  }else{
    elem.style.display = "none";
  }
}

function toggle_aside(){
  var elem = document.getElementById('myAccordion');
  if(!elem) return;
  if(elem.style.display == "none"){
    elem.style.display = "block";
  }else{
    elem.style.display = "none";
  }
}

function close_aside(){
  var elem = document.getElementById('myAccordion');
  if(!elem) return;
  elem.style.display = "none";
}
