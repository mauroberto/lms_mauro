var addModal = document.getElementById("trigger-modal");
var modal = document.getElementById("modal");
var modalOverlay = document.getElementById("modal-overlay");
var closeModal = document.getElementById("close-modal");


var modalf = function(){
  if(modal.style.display == "block"){
    modal.style.display = "none";
    modalOverlay.style.display = "none";
  }else{
    modal.style.display = "block";
    modalOverlay.style.display = "block";
  }
}

modalOverlay.addEventListener("click", modalf);

addModal.addEventListener("click", modalf);

closeModal.addEventListener("click", modalf);


var deletePost = function(postId){
  var result = confirm("Tem certeza que deseja excluir este post?");

  if(!result) return;

  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      var post = document.getElementById(postId+'');
      post.parentNode.removeChild(post);
    }
  };

  xmlhttp.open("DELETE", "http://rest.learncode.academy/api/mauro/redesocial/"+postId, true);
  xmlhttp.send();
};


var getFomattedDate = function(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!

  var yyyy = today.getFullYear();
  if(dd<10){
      dd='0'+dd
  }
  if(mm<10){
      mm='0'+mm
  }

  return dd+'/'+mm+'/'+yyyy;
};


var addPost = function(post){
  var template = [
    '<div class="post" id="'+post.id+'">',
      '<div class="right"><a class="delete" onclick="deletePost(\''+post.id.toString()+'\')"><i class="fa fa-trash"></i></a></div>',
      '<header>',
        '<div><a href="#">'+post.nome+'</a></div>',
        '<span class="date">'+post.data+'</span>',
      '</header>',
      '<article>',
        '<p>'+post.texto+'</p>',
      '</article>',
    '</div>'
  ].join('');

  document.getElementById("posts").innerHTML += template;
};

var loadPosts = function(callback){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      var json = JSON.parse(this.responseText);
      for(i in json){
        addPost(json[i]);
      }

      if(callback){
        callback();
      }
    }
  };

  xmlhttp.open("GET", "http://rest.learncode.academy/api/mauro/redesocial", true);
  xmlhttp.send();
};

var submitPost = function(e){
  e.preventDefault();
  var data = getFomattedDate();
  var nome = document.getElementById("nome").value || "";
  var texto = document.getElementById("texto").value || "";

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      addPost(JSON.parse(this.responseText));
      document.getElementById("nome").value = "";
      document.getElementById("texto").value = "";
      modalf();
    }
  };

  xmlhttp.open("POST", "http://rest.learncode.academy/api/mauro/redesocial", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("nome="+nome+"&texto="+texto+"&data="+data);

  return false;
};

document.getElementById("formPost").addEventListener("submit", submitPost);

window.onload = loadPosts;

var isRefreshing = false;
document.getElementById("refresh").addEventListener("click", function(){
  if(isRefreshing){
    return;
  }

  isRefreshing = true;
  document.getElementById("posts").innerHTML = "";
  loadPosts(function(){
    isRefreshing = false;
  });
});

var elems = document.querySelectorAll('.input-form input, .input-form textarea');

for(i in elems){
  elems[i].addEventListener("blur", function(){
    if(this.value && this.value.trim().length > 0){
      this.parentNode.className = "input-form active";
    }else{
      this.parentNode.className = "input-form";
    }
  });
}
