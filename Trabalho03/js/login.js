function fazerLogin(user){
  localStorage.setItem("user", JSON.stringify(user));
  $(".comLogin").show();
  $("#userEmail").text(user.email.split("@")[0]);
  $(".semLogin").hide();
  inicializarCarrinho();
}

function testarLogin(){
  var user = localStorage.getItem("user");
  if(user){
    user = JSON.parse(user);
    $(".comLogin").show();
    $(".semLogin").hide();
    console.log(user);
    $("#userEmail").text(user.email.split("@")[0]);
  }else{
    var filename = location.pathname.substr(location.pathname.lastIndexOf("/")+1);
    if(filename == "compras.html"){
      window.location = "index.html";
    }

    $(".semLogin").show();
    $(".comLogin").hide();
  }
}

$(function(){
  testarLogin();

  $("#logout").click(function(){
    localStorage.setItem("user", "");
    testarLogin();
  });

  $("#login").submit(function(){
    event.preventDefault();

    var $email = $("#email", this),
      $password = $("#password", this);

    var email = $email.val(),
      password = $password.val();


      if(email.trim().length == 0 || !validateEmail(email)){
        $email.parent('.form-group').addClass('has-error');
        $email.parent('.form-group').removeClass('has-success');
        return false;
      }else{
        $email.parent('.form-group').removeClass('has-error');
        $email.parent('.form-group').addClass('has-success');
      }

      if(password.length == 0){
        $password.parent('.form-group').addClass('has-error');
        $password.parent('.form-group').removeClass('has-success');
        return false;
      }else{
        $password.parent('.form-group').removeClass('has-error');
        $password.parent('.form-group').addClass('has-success');
      }

      var user = undefined;
      for(i in usuarios){
        if(usuarios[i].email == email){
          if(usuarios[i].password == password){
            user = usuarios[i];
          }
          break;
        }
      }

      if(user){
        $password.val("").parent('.form-group').removeClass('has-error has-success');
        $email.val("").parent('.form-group').removeClass('has-error has-success');
        $("#falhaNoLogin", this).addClass('hide');
        fazerLogin(user);
        return false;
      }else{
        $("#falhaNoLogin", this).removeClass('hide');
      }
  });
});
