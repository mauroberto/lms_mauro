var categorias = [
  {
    nome: "Smartphones",
    id: 1,
    cor:"red",
    produtos: [{
      id:1,
      nome:"Moto G4",
      thumb:"128365990P1.png",
      img:"128365990_1GG.png",
      marca: "Motorola",
      descricao:"Smartphone Moto G 4 Play Dual Chip Android 6.0 Tela 5\" 16GB Câmera 8MP - Preto",
      preco: 869.00
    },
    {
      id:2,
      nome: "Galaxy J5",
      thumb:"124132646P1.png",
      img: "124132646_1GG.png",
      marca: "Samsung",
      descricao:"Smartphone Samsung Galaxy J5 Duos Dual Chip Android 5.1 Tela 5\" 16GB 4G Wi-Fi Câmera 13MP - Dourado",
      preco: 843.33
    },
    {
      id:3,
      nome:"iPhone 6S Plus",
      thumb:"129252111P1.png",
      img: "129252111_1GG.jpg",
      marca: "Apple",
      descricao:"iPhone 6S Plus 32GB Dourado Tela 5,5\" IOS 4G Câmera 12MP - Apple",
      preco: 3599.99
    }]
  },
  {
    nome: "Smart TV",
    id:2,
    cor:"blue",
    produtos: [{
      id:4,
      nome:"Samsung UN32J4300AGXZD",
      thumb:"122701411P1.png",
      img:"122701411_1GG.png",
      marca: "Samsung",
      descricao:"Smart TV LED 32\" Samsung UN32J4300AGXZD HD com Conversor Digital 2 HDMI 1 USB Wi-Fi 120Hz",
      preco: 1399.99
    },
    {
      id: 5,
      nome: "Sony 3D XBR-55X905A",
      thumb:"114003699P1.png",
      img: "114003699_1GG.png",
      marca: "Sony",
      descricao:"Smart TV LED 55\" Sony 3D XBR-55X905A Ultra HD 4K 4 HDMI 3 USB Wi-Fi 960hz + 4 óculos 3D",
      preco: 8570.99
    },
    {
      id:6,
      nome:"LG 49UH6500",
      thumb:"126644653P1.png",
      img:"126644653_1GG.png",
      marca: "LG",
      descricao:"Smart TV LED 49\" LG 49UH6500 Ultra HD 4K com Conversor Digital Wi-Fi HDR Pro WiDi 2 USB 3 HDMI",
      preco: 3099.99
    }]
  },
  {
    nome: "Notbooks",
    id: 3,
    cor:"green",
    produtos: [{
      id:7,
      nome:"Dell Inspiron",
      thumb:"126897693P1.png",
      img: "126897693_1GG.jpg",
      marca: "Dell",
      descricao:"Notebook Dell Inspiron Special Edition i15-5557-a40 Intel Core i7 16GB (4GB de Memória Dedicada) 1TB 8GB SSD  LED Touch 15.6\" Windows 10  - Prata",
      preco: 4198.99
    },
    {
      id:8,
      nome: "Asus X555UB-BRA-XX274T",
      thumb:"127745448P1.png",
      img: "127745448_1GG.jpg",
      marca: "Asus",
      descricao:"Notebook Asus X555UB-BRA-XX274T Intel Core 6 i7 8GB (2GB Memória Dedicada)  1TB LED 15,6\" Windows 10 - Preto",
      preco: 3980.90
    },
    {
      id:9,
      nome:"Lenovo Ideapad 310",
      thumb:"128914434P1.png",
      img: "128914434_1GG.png",
      marca: "Lenovo",
      descricao:"Notebook Lenovo Ideapad 310 Intel Core 6 i7-6500u 8GB (2GB de Memória Dedicada) 1TB Tela LED 15\" Windows 10 - Prata",
      preco: 3199.99
    }]
  }
];

function pegarProdutoPorId(id){
  for(i in categorias){
    for(j in categorias[i].produtos){
      if(categorias[i].produtos[j].id == id){
        return categorias[i].produtos[j];
      }
    }
  }

  return null;
}
