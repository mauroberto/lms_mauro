function atualizarCarrinho(){
  var $carrinho = $("#carrinho");
  $carrinho.empty();

  var template = [
    '<div class="container">',
      '<table class="table table-hover table-bordered">',
        '<tbody id="produtos_cart">',
          '<tr>',
            '<th>Item</th>',
            '<th>Quantidade</th>',
            '<th>Preço Unitário</th>',
            '<th>Preço Total</th>',
          '</tr>',
        '</tbody>',
      '</table>',
    '</div>'
  ].join('');

  $carrinho.append(template);

  var $produtos_cart = $("#produtos_cart");

  var user = localStorage.getItem("user");

  if(user){
    user = JSON.parse(user);
  }

  var cart = localStorage.getItem("cart_"+user.id);

  var total = 0;

  if(cart){
    cart = JSON.parse(cart);
  }

  if(!cart.length){
    $carrinho.html("<p>Seu carrinho está vazio</p><a href='index.html' class='btn btn-primary'>Adicionar Produtos</a>");
    return;
  }

  for(i in cart){
    total += cart[i].produto.preco * cart[i].qtd;
    template = [
      '<tr>',
        '<td>'+cart[i].produto.nome+'</td>',
        '<td>'+cart[i].qtd+'</td>',
        '<td>R$ '+cart[i].produto.preco.formatMoney(2, ',', '.')+'</td>',
        '<td>R$ '+(cart[i].produto.preco * cart[i].qtd).formatMoney(2, ',', '.')+'</td>',
      '</tr>'
    ].join('');

    $produtos_cart.append(template);
  }

  template = [
    '<tr>',
      '<th colspan="3"><span class="pull-right">Total</span></th>',
      '<th>R$ '+total.formatMoney(2, ',', '.')+'</th>',
    '</tr>',
    '<tr>',
      '<td><a href="index.html" class="btn btn-primary">Continuar Comprando</a></td>',
      '<td colspan="3"><a id="checkout" class="pull-right btn btn-success">Finalizar</a></td>',
    '</tr>'
  ].join('');

  $produtos_cart.append(template);
}

var compras = [];


function mostrarCompra(compra){
  var $compras = $("#compras");

  var template = [
    '<div class="container" id="compra'+compra.id+'">',
      '<div>Data: '+getFomattedDate(compra.data)+'</div>',
      '<table class="table table-hover table-bordered">',
        '<tbody id="produtos_cart'+compra.id+'">',
          '<tr>',
            '<th>Item</th>',
            '<th>Quantidade</th>',
            '<th>Preço Unitário</th>',
            '<th>Preço Total</th>',
          '</tr>',
        '</tbody>',
      '</table>',
    '</div>'
  ].join('');

  $compras.prepend(template);

  var $produtos_cart = $("#produtos_cart"+compra.id);

  var cart = [];
  var i = 0;

  for(i in compra.produtos){
    var produto = pegarProdutoPorId(compra.produtos[i].id);
    if(produto){
      produto.preco = compra.produtos[i].preco;
      cart.push({
        qtd: compra.produtos[i].qtd,
        produto: produto
      });
    }
  }

  console.log(cart);

  if(!cart.length){
    $("compra"+compra.id).remove();
    return false;
  }

  for(i in cart){
    console.log(cart[i].produto); //TODO: PAREI AQUI
    template = [
      '<tr>',
        '<td>'+cart[i].produto.nome+'</td>',
        '<td>'+cart[i].qtd+'</td>',
        '<td>R$ '+parseInt(cart[i].produto.preco).formatMoney(2, ',', '.')+'</td>',
        '<td>R$ '+(parseInt(cart[i].produto.preco) * parseInt(cart[i].qtd)).formatMoney(2, ',', '.')+'</td>',
      '</tr>'
    ].join('');

    $produtos_cart.append(template);
  }

  template = [
    '<tr>',
      '<th colspan="3"><span class="pull-right">Total</span></th>',
      '<th>R$ '+parseInt(compra.total).formatMoney(2, ',', '.')+'</th>',
    '</tr>'
  ].join('');

  $produtos_cart.append(template);
}


$.get("http://rest.learncode.academy/api/mauro/compras", function(response){
  var user = localStorage.getItem("user");
  if(user){
    user = JSON.parse(user);
  }else {
    return;
  }

  for(i in response){
    if(response[i].userId == user.id){
      compras.push(response[i]);
      mostrarCompra(response[i]);
      //$.ajax({type:"DELETE", url:"http://rest.learncode.academy/api/mauro/compras/"+response[i].id});
    }
  }
});


$(function(){
  atualizarCarrinho();

  $("#checkout").click(function(){
    var user = localStorage.getItem("user");
    if(user){
      user = JSON.parse(user);
    }else {
      return false;
    }

    var cart = localStorage.getItem("cart_"+user.id);

    if(cart){
      cart = JSON.parse(cart);
    }else{
      return false;
    }

    var total = 0, produtos = [];
    for (i in cart){
      total += cart[i].qtd*cart[i].produto.preco;
      produtos.push({
        preco: cart[i].produto.preco,
        qtd: cart[i].qtd,
        id: cart[i].produto.id
      });
    }

    var compra = {
      userId: user.id,
      total: total,
      produtos: produtos,
      data: new Date()
    };

    $.ajax({
      url:"http://rest.learncode.academy/api/mauro/compras",
      type:"POST",
      data: compra,
      success: function(response){
        console.log(response);
        localStorage.setItem("cart_"+user.id, []);
        compras.push(response);
        mostrarCompra(response);
        atualizarCarrinho();
        mostrarCarrinho(user);
        $.notify({
          message: 'Compra realizada com sucesso!'
        },{
          z_index: 2040,
          type: 'success',
          placement: {
            from: "top",
            align: "left"
           }
        });
      },
      error: function(error){
        console.log(error);
      }
    });

  });

});
