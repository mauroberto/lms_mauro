$(function(){
  var controller = new ScrollMagic.Controller();

  var nomeProdutos = [];

  function addProduto(produto, categoria){
    var $produtos = $("#produtos"+categoria.id);

    var template = [
        '<div class="col-xs-12 col-sm-6 col-md-4 produto animated" id="produto'+produto.id+'" title="'+produto.nome+'">',
          '<div class="thumbnail" >',
            '<h4 class="text-center"><span class="label label-info">'+produto.marca+'</span></h4>',
            '<img style="padding:10px;" src="img/'+produto.img+'" class="img-responsive">',
            '<div class="caption">',
              '<div class="row">',
                '<div class="col-xs-12">',
                  '<h3>'+produto.nome+'</h3>',
                '</div>',
                '<div class="col-xs-12 descricao">'+produto.descricao+'</div>',
                '<div class="col-xs-12 price">',
                  '<h3><label>R$ '+produto.preco.formatMoney(2, ',', '.')+'</label></h3>',
                '</div>',
              '</div>',
              '<div class="row">',
                '<form novalidate class="comLogin">',
                  '<div class="col-md-6 col-sm-6 col-xs-6 form-group">',
                    '<input type="number" placeholder="quantidade" class="form-control" name="qtd" min="1" value="1">',
                  '</div>',
                  '<div class="col-md-6 col-sm-6 col-xs-6">',
                    '<button type="submit" href="#" class="btn btn-primary btn-product"><span class="glyphicon glyphicon-shopping-cart"></span></button>',
                  '</div>',
                '</form>',
              '</div>',
            '</div>',
          '</div>',
        '</div>'
    ].join('');

    nomeProdutos.push({label: produto.nome, value: produto.nome, id: produto.id});

    $produtos.append(template);

    /*new ScrollMagic.Scene({triggerElement: "#produto"+produto.id})
  					.setClassToggle("#produto"+produto.id, "bounceIn") // add class toggle
  					.addTo(controller);*/

    /*new ScrollMagic.Scene({triggerElement: "#produto"+produto.id, duration: 0})
      .on("enter", function () {
          $("#produto"+produto.id).removeClass('hide').addClass('bounceIn');
      })
    .addTo(controller);*/


    $("form", "#produto"+produto.id).submit(function(){
      event.preventDefault();
      var item = {
        produto: produto
      };

      var $qtd = $("input[name='qtd']", this);
      var qtd = $qtd.val();

      if(isNaN(qtd) || parseInt(qtd) != qtd || parseInt(qtd) < 1){
        $qtd.parent('.form-group').addClass('has-error');
        return false;
      }else{
        $qtd.parent('.form-group').removeClass('has-error');
      }

      item.qtd = parseInt(qtd);

      var user = localStorage.getItem("user");
      if(user){
        user = JSON.parse(user);
        if(addItem(item, user)){
          $.notify({
            // options
            message: 'Produto adicionado ao carrinho!'
          },{
            // settings
            z_index: 2040,
            type: 'success',
            placement: {
		          from: "top",
		          align: "left"
	           }
          });
        }
      }
    });
  }


  var $produtos = $("#produtos");
  var $categorias = $("#categorias");

  function addCategoria(categoria){

    var template = [
      '<div class="categoria row" id="produtos'+categoria.id+'">',
	      '<div><h2>'+categoria.nome+'</h2></div>',
      '</div>'
    ].join('');

    $produtos.append(template);

    var template = '<li id="categoria'+categoria.id+'" ><a href="#produtos'+categoria.id+'">'+categoria.nome+'</a></li>';

    $categorias.append(template);

    for(i in categoria.produtos){
      addProduto(categoria.produtos[i], categoria);
    }

    var tween = TweenMax.staggerFromTo("#produtos"+categoria.id+ " .produto", 1, {opacity: 0}, {opacity: 1, ease: Back.easeOut, className: "+=bounceIn"}, 0.3);
    var scene = new ScrollMagic.Scene({triggerElement: "#produtos"+categoria.id, duration: 300, offset: -150})
							.setTween(tween)
							.addTo(controller);

    //var duration = parseInt($("#produtos"+categoria.id).css("height")) + parseInt($("#produtos"+categoria.id).css("padding-top")) + parseInt($("#produtos"+categoria.id).css("padding-bottom"));
    //console.log(duration);

    var scene = new ScrollMagic.Scene({triggerElement: "#produtos"+categoria.id, duration: 563}).setClassToggle("#categoria"+categoria.id, "active").addTo(controller);
    //new ScrollMagic.Scene({triggerElement: "#pin"+categoria.id}).addIndicators().setClassToggle("#categoria"+categoria.id, "active").addTo(controller);

    $(window).resize(function(){
      scene.refresh();
    });
  }

  for(i in categorias){
    addCategoria(categorias[i]);
  }

var scene = new ScrollMagic.Scene({triggerElement: "#categorias", triggerHook: "onLeave", offset: -80}).setPin("#categorias").on("enter", function(){console.log("pin");}).addTo(controller);

var tween = TweenMax.staggerFromTo("#brand", 2, {fontSize: 63, zIndex: 2000}, {fontSize: 18, ease: Back.linear});

var scene2 = new ScrollMagic.Scene({triggerElement: "#brand", triggerHook:"onLeave", duration: 230}).setTween(tween).addTo(controller);

var scene3 = new ScrollMagic.Scene({triggerElement: "#brand", triggerHook: "onLeave"}).setClassToggle("#brand", "navbar-brand").setPin("#brand").addTo(controller);

var scene4 = new ScrollMagic.Scene({triggerElement: "#searchBar", triggerHook: "onLeave", offset: 80}).setClassToggle("#searchBarNav", "show").addTo(controller);


$(window).resize(function(){
  console.log("asd");
  scene.refresh();
  scene2.refresh();
  scene3.refresh();
  scene4.refresh();
});
  //var controller = new ScrollMagic.Controller();

	// build scene

  //var cont = 0;
	//var scene = new ScrollMagic.Scene({triggerElement: "#produtos", triggerHook: "onLeave"})
		//			.addTo(controller)
			//		.on("enter", function (e) {
        //    var cat = cont/categorias.length;
					//});

  controller.scrollTo(function (newpos, offset) {
		TweenMax.to(window, 0.5, {scrollTo: {y: newpos + offset}});
	});

	//  bind scroll to anchor links
	$(document).on("click", "a[href^='#']", function (e) {
		var id = $(this).attr("href");
		if ($(id).length > 0) {
			e.preventDefault();

			// trigger scroll
			controller.scrollTo(id, -50);

				// if supported by the browser we can even update the URL.
			if (window.history && window.history.pushState) {
				history.pushState("", document.title, id);
			}
		}
	});

  var autocomplete = {
      source: nomeProdutos,
      change: function( event, ui ) {
        console.log(ui);
        if(ui.item)
          controller.scrollTo("#produto"+ui.item.id);
      }
  };

  $("#searchBar input[type='text']").autocomplete(autocomplete);
  $("#searchBarNav input[type='text']").autocomplete(autocomplete);

  function buscar(){
    event.preventDefault();
    var val = $("input[type='text']", this).val().toLowerCase();

    for(i in nomeProdutos){
      if(nomeProdutos[i].value.toLowerCase() == val){
        var $produto = $(".thumbnail", "#produto"+nomeProdutos[i].id);
        $produto.removeClass("searchResult");
        controller.scrollTo("#produto"+nomeProdutos[i].id, -100);
        $produto.addClass("searchResult");
        break;
      }
    }
  };


  $("#searchBarNav").submit(buscar);
  $("#searchBar form").submit(buscar);
});
