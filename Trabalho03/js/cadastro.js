$(function(){
  $("#cadastroForm").submit(function(){
    event.preventDefault();

    var $email = $("#emailCadastro", this),
      $password = $("#passwordCadastro", this),
      $confirmPassword = $("#confirmPassword", this);
    var email = $email.val(),
      password = $password.val(),
      confirmPassword = $confirmPassword.val();


    if(email.trim().length == 0 || !validateEmail(email)){
      $email.parent('.form-group').addClass('has-error');
      $email.parent('.form-group').removeClass('has-success');
      return false;
    }else{
      $email.parent('.form-group').removeClass('has-error');
      $email.parent('.form-group').addClass('has-success');
    }

    if(password.length == 0){
      $password.parent('.form-group').addClass('has-error');
      $password.parent('.form-group').removeClass('has-success');
      return false;
    }else{
      $password.parent('.form-group').removeClass('has-error');
      $password.parent('.form-group').addClass('has-success');
    }

    if(confirmPassword.length == 0 || confirmPassword != password){
      $confirmPassword.parent('.form-group').addClass('has-error');
      $confirmPassword.parent('.form-group').removeClass('has-success');
      return false;
    }else{
      $confirmPassword.parent('.form-group').removeClass('has-error');
      $confirmPassword.parent('.form-group').addClass('has-success');
    }

    var user = pegarUsuarioPorEmail(email);
    if(user){
      $.notify({
        // options
        message: 'Já existe um usuário cadastrado com esse email!'
      },{
        // settings
        z_index: 2040,
        type: 'danger',
        placement: {
          from: "top",
          align: "left"
         }
      });
      return false;
    }

    $.ajax({
      type: "POST",
      url: "http://rest.learncode.academy/api/mauro/usuarios",
      data:{email: email, password: password},
      dataType: "JSON",
      success:function(response){
        $("#cadastroModal").modal('hide');
        $.notify({
        	// options
        	message: 'Cadastro realizado com sucesso!'
        },{
        	// settings
          z_index: 2040,
        	type: 'success',
          placement: {
            from: "top",
            align: "left"
           }
        });

        $email.val("").parent('.form-group').removeClass('has-success has-error');
        $password.val("").parent('.form-group').removeClass('has-success has-error');
        $confirmPassword.val("").parent('.form-group').removeClass('has-success has-error');

        usuarios.push(response);
        fazerLogin(response);
      },
      error:function(response){
        $("#cadastroModal").modal('hide');
        $.notify({
        	// options
        	message: 'Ops! Parece que ocorreu algum problema durante o cadastro!'
        },{
        	// settings
          z_index: 2040,
        	type: 'danger',
          placement: {
            from: "top",
            align: "left"
           }
        });
      }
    })

  });
});
