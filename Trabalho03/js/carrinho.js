var $cart_dropdown = $("#cart-dropdown");
var $cart_itens = $("#cart_itens");

function mostrarCarrinho(user){
  var cart = localStorage.getItem("cart_"+user.id);
  var l = 0;
  if(cart){
      cart = JSON.parse(cart);
      l = cart.length;
  }


  $cart_itens.html("<span class='glyphicon glyphicon-shopping-cart'></span> "+l+" - Ite"+((l == 1) ? "m" : "ns")+" <span class='caret'></span>");

  if(!l){
    $cart_dropdown.html("<li class='vazio'>Seu Carrinho está vazio</li>");
    return false;
  }

  $cart_dropdown.empty();

  for(i in cart){
    mostrarItem(cart[i]);
  }

  $cart_dropdown.append('<li class="divider"></li><a href="compras.html" style="margin-right:5px;" class="btn btn-primary pull-right" href="">Finalizar</a>');
}

function removerItem(item, user){
  var cart = localStorage.getItem("cart_"+user.id);
  if(cart){
    cart = JSON.parse(cart);

    for(i in cart){
      if(cart[i].produto.id == item.produto.id){
        cart.splice(i, 1);
        localStorage.setItem("cart_"+user.id, JSON.stringify(cart));
        mostrarCarrinho(user);
        $.notify({
          // options
          message: 'Item removido do carrinho!'
        },{
          // settings
          z_index: 2040,
          type: 'success',
          placement: {
            from: "top",
            align: "left"
           }
        });
        break;
      }
    }

  }
}

function mostrarItem(item){
  var template = [
    '<li id="cart_produto'+item.produto.id+'">',
      '<span class="item">',
        '<span class="item-right">',
            '<button class="btn btn-xs btn-danger pull-right">x</button>',
        '</span>',
        '<span class="item-left">',
            '<img class="thumb" src="img/thumbnails/'+item.produto.thumb+'" alt="" />',
            '<span class="item-info">',
                '<span>'+item.qtd+' x '+item.produto.nome+'</span>',
                '<span>R$ '+(item.produto.preco*item.qtd).formatMoney(2, ',', '.')+'</span>',
            '</span>',
        '</span>',
      '</span>',
    '</li>'
  ].join('');

  $cart_dropdown.append(template);

  $(".btn-danger", "#cart_produto"+item.produto.id).click(function(){
    var user = localStorage.getItem("user");
    if(user){
      user = JSON.parse(user);
      removerItem(item, user);
      var filename = location.pathname.substr(location.pathname.lastIndexOf("/")+1);
      if(filename == "compras.html"){
        atualizarCarrinho();
      }
    }
  });
}

function addItem(item, user){
  var cart  = localStorage.getItem("cart_"+user.id);
  cart = JSON.parse(cart);

  for(i in cart){
    if(cart[i].produto.id == item.produto.id){
      cart[i].qtd = parseInt(cart[i].qtd) + parseInt(item.qtd);
      localStorage.setItem("cart_"+user.id, JSON.stringify(cart));
      mostrarCarrinho(user);
      return true;
    }
  }

  cart.push(item);

  console.log(cart);
  localStorage.setItem("cart_"+user.id, JSON.stringify(cart));
  mostrarCarrinho(user);
  return true;
}

function inicializarCarrinho(){
  //localStorage.setItem("cart_"+user.id, "");
  var user = localStorage.getItem("user");
  if(user){
    user = JSON.parse(user);
  }else{
    return;
  }

  if(!localStorage.getItem("cart_"+user.id)){
    localStorage.setItem("cart_"+user.id, "[]");
  }

  mostrarCarrinho(user);
}

inicializarCarrinho();
